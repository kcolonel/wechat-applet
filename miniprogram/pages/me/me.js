// pages/me/me.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    login:false,
    avatarUrl:'',
    nickName:'',
    cellList:[
      {
        url:'../../images/Recording.png',
        text:'我的记录',
        page:'../myrecord/myrecord'
      },
      {
        url:'../../images/collection_.png',
        text:'我的收藏',
        page:'../myCollection/myCollection'
      },
      {
        url:'../../images/Information.png',
        text:'我的信息',
        page:'../myInfo/myInfo'
      },
      {
        url:'../../images/quit.png',
        text:'退出登录'
      }
    ]
  },
  toDetail(e){
    const{ page }= e.currentTarget.dataset;
    wx.navigateTo({
      url: page,
    })
  },

  toLogin()
  {
    wx.getUserProfile({
      desc: '获取用户信息',
      success:(res)=>{
        console.log(res);
        const{ userInfo: { avatarUrl,nickName } }=res;
        const userInfo={
          avatarUrl,
          nickName
        }
        
        wx.setStorageSync('userInfo', userInfo);
        wx.setStorageSync('login', true);
        this.setData({
          login:true
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const login = wx.getStorageSync('login');
    const userInfo = wx.getStorageSync('userInfo');
    if(userInfo){
      const {avatarUrl,nickName} = userInfo;
      this.setData({
        avatarUrl:avatarUrl,
        nickName:nickName
      })
    }
    this.setData({
      login:!!login
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    if(typeof this.getTabBar==='function' && this.getTabBar())
    {
      this.getTabBar().setData({
        select:4
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})