// pages/collection/collection.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
      tabList:["食品","动作"],
      news_s_:['../../images/apple.jpg','../../images/broccoli.jpg'],//首页健康食品数量,
      news_s_name:['苹果','西蓝花'],//首页食品名字
      news_s_energy:['60','36'],//食品的热量
      news_s_protein:['0.2','4.1'],//食品的蛋白质含量
      news_s_jj:['aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa','bbbbbbbbbbbbbbbbbbbbbbbbbbbb'],//食品的详细描述
      news_s_time:['6/20','6/18'],//食品发布时间
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    if(typeof this.getTabBar==='function' && this.getTabBar())
    {
      this.getTabBar().setData({
        select:3
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})